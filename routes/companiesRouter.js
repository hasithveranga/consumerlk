var express = require("express");
var bodyParser = require("body-parser");
var passport = require("passport");
var auth = require("../auth");
var cors = require("./cors");

var Company = require("../models/company");

var companyRouter = express.Router();

// Use body parser to parse JSON
companyRouter.use(bodyParser.json());

// HTTP method implementations for /companies route.
companyRouter.get("/", cors.cors, auth.verifyUser, (req, res, next) => {
  Company.find({})
    .populate("reviews.user")
    .then(
      (companies) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "application/json");
        res.json(companies);
      },
      (err) => next(err)
    )
    .catch((err) => next(err));
});

// HTTP method implementations for /companies/:companyId/reviews route.
companyRouter.post("/:companyId/reviews", cors.corsWithOptions, auth.verifyUser, (req, res, next) => {
  Company.findById(req.params.companyId)
  .then((company) => {
      if (company != null) {
          req.body.user = req.user._id;
          company.reviews.push(req.body);
          company.save()
          .then((company) => {
            Company.findById(company._id)
              .populate('comments.author')
              .then((company) => {
                  res.statusCode = 200;
                  res.setHeader('Content-Type', 'application/json');
                  res.json(company);
              })            
          }, (err) => next(err));
      }
      else {
          err = new Error('Company ' + req.params.companyId + ' not found');
          err.status = 404;
          return next(err);
      }
  }, (err) => next(err))
  .catch((err) => next(err));
});

// HTTP method implementations for /users/signup route.
companyRouter.post("/signup", cors.corsWithOptions, (req, res, next) => {
  Company.register(
    new Company({ username: req.body.username }),
    req.body.password,
    (err, company) => {
      if (err) {
        res.statusCode = 500;
        res.setHeader("Content-Type", "application/json");
        res.json({ err: err });
      } else {
        if (req.body.name) company.name = req.body.name;
        if (req.body.description) company.description = req.body.description;
        company.save((err, company) => {
          if (err) {
            res.statusCode = 500;
            res.setHeader("Content-Type", "application/json");
            res.json({ err: err });
            return;
          }
          passport.authenticate("company-local")(req, res, () => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json({ success: true, status: "Registration Successful!" });
          });
        });
      }
    }
  );
});

// HTTP method implementations for /users/login route.
companyRouter.post(
  "/login",
  cors.corsWithOptions,
  passport.authenticate("company-local"),
  (req, res) => {
    var token = authenticate.getToken({ _id: req.user._id });
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");
    res.json({
      success: true,
      token: token,
      status: "You are successfully logged in as a company!",
    });
  }
);

// HTTP method implementations for /users/logout route.
companyRouter.get("/logout", cors.corsWithOptions, (req, res) => {
  if (req.session) {
    req.session.destroy();
    res.clearCookie("session-id");
    res.redirect("/");
  } else {
    var err = new Error("You are not logged in!");
    err.status = 403;
    next(err);
  }
});

// Export module
module.exports = companyRouter;
