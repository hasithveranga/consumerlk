var express = require("express");
var bodyParser = require("body-parser");
var passport = require("passport");
var authenticate = require("../auth");
var cors = require("./cors");

var User = require("../models/user");

var userRouter = express.Router();

// Use body parser to parse JSON
userRouter.use(bodyParser.json());

userRouter.get("/", cors.cors, (req, res, next) => {
  res.json("{test}");
});

// HTTP method implementations for /users/signup route.
userRouter.post("/signup", cors.corsWithOptions, (req, res, next) => {
  User.register(
    new User({ username: req.body.username }),
    req.body.password,
    (err, user) => {
      if (err) {
        res.statusCode = 500;
        res.setHeader("Content-Type", "application/json");
        res.json(err);
      } else {
        if (req.body.firstname) user.firstname = req.body.firstname;
        if (req.body.lastname) user.lastname = req.body.lastname;
        user.save((err, user) => {
          if (err) {
            res.statusCode = 500;
            res.setHeader("Content-Type", "application/json");
            res.json({ err: err });
            return;
          }
          passport.authenticate("user-local")(req, res, () => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json({ success: true, status: "Registration Successful!" });
          });
        });
      }
    }
  );
});

// HTTP method implementations for /users/login route.
userRouter.post(
  "/login",
  cors.corsWithOptions,
  passport.authenticate("user-local"),
  (req, res) => {
    var token = authenticate.getToken({ _id: req.user._id });
    res.statusCode = 200;
    res.setHeader("Content-Type", "application/json");
    res.json({
      success: true,
      token: token,
      status: "You are successfully logged in!",
    });
  }
);

// HTTP method implementations for /users/logout route.
userRouter.get("/logout", cors.corsWithOptions, (req, res) => {
  if (req.session) {
    req.session.destroy();
    res.clearCookie("session-id");
    res.redirect("/");
  } else {
    var err = new Error("You are not logged in!");
    err.status = 403;
    next(err);
  }
});

// Export module
module.exports = userRouter;
