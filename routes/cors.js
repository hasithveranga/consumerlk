const express = require("express");
const cors = require("cors");

// Allowable origins.
const whitelist = ["http://localhost:3000", "https://localhost:3443"];

// Chech if the origin is allowable.
var corsOptionsDelegate = (req, callback) => {
  var corsOptions;
  console.log(req.header("Origin"));
  if (whitelist.indexOf(req.header("Origin")) !== -1) {
    corsOptions = { origin: true };
  } else {
    corsOptions = { origin: false };
  }
  callback(null, corsOptions);
};

// Export cors configurations.
exports.cors = cors();
exports.corsWithOptions = cors(corsOptionsDelegate);
