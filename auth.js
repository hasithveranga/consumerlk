var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var JwtStrategy = require("passport-jwt").Strategy;
var ExtractJwt = require("passport-jwt").ExtractJwt;
var jwt = require("jsonwebtoken");

var User = require("./models/user");
var Company = require("./models/company");

var config = require("./config.js");

// Build local strategy
passport.use("user-local", new LocalStrategy(User.authenticate()));

// Serialize / Deserialize user to store in the sesion
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Build local strategy
passport.use("company-local", new LocalStrategy(Company.authenticate()));

// Serialize / Deserialize company to store in the sesion
passport.serializeUser(Company.serializeUser());
passport.deserializeUser(Company.deserializeUser());

// Sign the token and return
exports.getToken = function (user) {
  return jwt.sign(user, config.secretKey, { expiresIn: 3600 });
};

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.secretKey;

// Build JWT strategy
passport.use(
  "user-jwt",
  new JwtStrategy(opts, (jwt_payload, done) => {
    console.log("JWT payload: ", jwt_payload);
    User.findOne({ _id: jwt_payload._id }, (err, user) => {
      if (err) {
        return done(err, false);
      } else if (user) {
        return done(null, user);
      } else {
        return done(null, false);
      }
    });
  })
);

// Export verifyUser
exports.verifyUser = passport.authenticate("user-jwt", { session: false });

// Build JWT strategy
passport.use(
  "company-jwt",
  new JwtStrategy(opts, (jwt_payload, done) => {
    console.log("JWT payload: ", jwt_payload);
    Company.findOne({ _id: jwt_payload._id }, (err, company) => {
      if (err) {
        return done(err, false);
      } else if (company) {
        return done(null, company);
      } else {
        return done(null, false);
      }
    });
  })
);

// Export verifyUser
exports.verifyCompany = passport.authenticate("company-jwt", {
  session: false,
});
