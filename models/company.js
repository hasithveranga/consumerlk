var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var passportLocalMongoose = require("passport-local-mongoose");

// Build user schema
var Review = new Schema(
  {
    image: {
      type: String,
      default: "",
    },
    comment: {
      type: String,
      default: "",
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

// Build user schema
var Company = new Schema(
  {
    name: {
      type: String,
      default: "",
    },
    description: {
      type: String,
      default: "",
    },
    logo: {
      type: String,
      default: "",
    },
    username: {
      type: String,
      default: "",
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: "Category",
    },
    reviews: [Review],
  },
  {
    timestamps: true,
  }
);

// Apply passport local mongoose plugin
Company.plugin(passportLocalMongoose);

// Build review model and export
module.exports = mongoose.model("Company", Company);
