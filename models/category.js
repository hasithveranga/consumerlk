var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// Build category schema
var Category = new Schema(
  {
    name: {
      type: String,
      default: "",
    },
    description: {
      type: String,
      default: "",
    },
  },
  {
    timestamps: true,
  }
);

// Build category model and export
module.exports = mongoose.model("Category", Category);
